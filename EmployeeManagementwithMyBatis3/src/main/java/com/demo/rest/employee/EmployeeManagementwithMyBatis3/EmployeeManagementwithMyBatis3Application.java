package com.demo.rest.employee.EmployeeManagementwithMyBatis3;

import org.apache.ibatis.type.MappedTypes;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

import com.demo.rest.employee.vo.Employee;

@SpringBootApplication
@MapperScan("com.demo.rest.employee.mapper")
@MappedTypes(value = { Employee.class })
@EnableAutoConfiguration(exclude = { DataSourceAutoConfiguration.class })
public class EmployeeManagementwithMyBatis3Application {

	public static void main(String[] args) {
		SpringApplication.run(EmployeeManagementwithMyBatis3Application.class, args);
	}

}
