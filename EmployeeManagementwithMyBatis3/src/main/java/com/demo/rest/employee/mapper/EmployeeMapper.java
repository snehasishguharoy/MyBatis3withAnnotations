/**
 * 
 */
package com.demo.rest.employee.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import com.demo.rest.employee.vo.Employee;

/**
 *
 */
// @Mapper
public interface EmployeeMapper {

	@Select("select * from Employee where employeeid=#{eid}")
	Employee getById(Integer id);

	@Select("select * from Employee")
	@Results({ @Result(id = true, column = "employeeid", property = "eid"),
			@Result(column = "lastname", property = "lname"), @Result(column = "firstname", property = "fname"),
			@Result(column = "address", property = "address"), @Result(column = "doj", property = "doj") })
	List<Employee> getAll();

}
