/**
 * 
 */
package com.demo.rest.employee.config;

import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.managed.ManagedTransactionFactory;

import com.demo.rest.employee.mapper.EmployeeMapper;

/**
 * @author sony
 *
 */
public class DataSourceFactory {

	public static DataSource getDataSource() {
		String jndiName = "java:comp/env/jdbc/Oracle";
		try {
			InitialContext context = new InitialContext();
			DataSource dataSource = (DataSource) context.lookup(jndiName);
			return dataSource;
		} catch (Exception e) {
			System.out.println(e);
		}
		return null;
	}

	public static SqlSessionFactory getSqlSessionFactory() {
		SqlSessionFactory sessionFactory = null;
		try {
			DataSource dataSource = DataSourceFactory.getDataSource();
			TransactionFactory factory = new ManagedTransactionFactory();
			Environment environment = new Environment("development", factory, dataSource);
			Configuration configuration = new Configuration(environment);
			configuration.getTypeAliasRegistry().registerAliases("com.demo.rest.employee.vo");
			configuration.addMapper(EmployeeMapper.class);
			sessionFactory = new SqlSessionFactoryBuilder().build(configuration);

		} catch (Exception e) {
			System.out.println(e);
		}
		return sessionFactory;
	}

}
