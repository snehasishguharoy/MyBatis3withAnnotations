/**
 * 
 */
package com.demo.rest.employee.EmployeeManagementwithMyBatis3.controller;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.rest.employee.config.DataSourceFactory;
import com.demo.rest.employee.mapper.EmployeeMapper;
import com.demo.rest.employee.vo.Employee;

/**
 * @author sony
 *
 */
@RestController
@RequestMapping("/rest")
public class EmpCtlr {
	SqlSession session = DataSourceFactory.getSqlSessionFactory().openSession();

	@GetMapping("/getemployeebyid/{id}")
	public Employee getById(@PathVariable("id") Integer id) throws Exception {

		EmployeeMapper employeeMapper = session.getMapper(EmployeeMapper.class);

		return employeeMapper.getById(id);
	}

	@GetMapping("/all")
	public List<Employee> getAll() throws Exception {

		EmployeeMapper employeeMapper = session.getMapper(EmployeeMapper.class);
		return employeeMapper.getAll();
	}

}
