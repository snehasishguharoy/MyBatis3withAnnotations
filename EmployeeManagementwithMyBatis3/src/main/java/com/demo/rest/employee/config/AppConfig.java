/**
 * 
 */
package com.demo.rest.employee.config;

import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.catalina.Context;
import org.apache.catalina.startup.Tomcat;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.tomcat.util.descriptor.web.ContextResource;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jndi.JndiObjectFactoryBean;

import com.demo.rest.employee.EmployeeManagementwithMyBatis3.EmployeeManagementwithMyBatis3Application;

/**
 *
 */

@Configuration
@ComponentScan(basePackages = "com.demo.rest.employee")
public class AppConfig extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(EmployeeManagementwithMyBatis3Application.class);
	}

	@Bean
	public TomcatEmbeddedServletContainerFactory tomcatFactory() {
		return new TomcatEmbeddedServletContainerFactory() {

			protected TomcatEmbeddedServletContainer getTomcatEmbeddedServletContainer(Tomcat tomcat) {
				tomcat.enableNaming();
				return super.getTomcatEmbeddedServletContainer(tomcat);
			}

			@Override
			protected void postProcessContext(Context context) {

				// Adding the JNDI Details for the Oracle
				ContextResource resource = new ContextResource();
				resource.setName("jdbc/Oracle");
				resource.setType(org.apache.tomcat.jdbc.pool.DataSource.class.getName());
				resource.setProperty("driverClassName", "org.postgresql.Driver");
				resource.setProperty("url", "jdbc:postgresql://localhost:5432/postgres");
				resource.setProperty("username", "postgres");
				resource.setProperty("password", "password");
				context.getNamingResources().addResource(resource);
			}
		};

	}

//	@Bean
//	public org.apache.tomcat.jdbc.pool.DataSource dataSource() throws IllegalArgumentException, NamingException {
//		JndiObjectFactoryBean bean = new JndiObjectFactoryBean();
//		bean.setJndiName("java:comp/env/jdbc/Oracle");
//		bean.setProxyInterface(org.apache.tomcat.jdbc.pool.DataSource.class);
//		bean.setLookupOnStartup(false);
//		bean.afterPropertiesSet();
//		System.out.println();
//		return (org.apache.tomcat.jdbc.pool.DataSource) bean.getObject();
//	}



}
